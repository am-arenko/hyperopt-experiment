### Experiments using hyperopt package with Imbalance price data

Tutorials used and others:
- http://hyperopt.github.io/hyperopt/ (Documentation - not very user friendly)

- https://github.com/hyperopt/hyperopt/blob/master/hyperopt/plotting.py (Documentation for hyperopt.plotting)

- https://github.com/hyperopt/hyperopt/wiki/FMin 

- https://www.analyticsvidhya.com/blog/2020/09/alternative-hyperparameter-optimization-technique-you-need-to-know-hyperopt/

- https://www.kaggle.com/inspector/keras-hyperopt-example-sketch (to use with Keras)

- https://stackoverflow.com/questions/43533610/how-to-use-hyperopt-for-hyperparameter-optimization-of-keras-deep-learning-netwo (alternatives to use with Keras)
