"""
Hyperopt tuning experiment adapted to imbalance price predictions w/ PLOTTING OPTIONS

NOTE: Plotly might be hard to read if some parameters are uniformly distributed while others
have a log uniform distribution (could be fixed). Make sure plotly is plotting the actual values tested
for each parameter and not the index from the distribution space...

"""

#  %%
import numpy as np
import pandas as pd
from hyperopt import hp, fmin, tpe, pyll, STATUS_OK, Trials
from hyperopt import space_eval, plotting
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error as mae
from sklearn.ensemble import GradientBoostingRegressor
import matplotlib.pyplot as plt
import plotly.express as px

# DATA
TARGET = "derived_system_wide_data_systemSellPrice"

# less data given to reduce processing time
data = pd.read_csv("final_dataset_18_19_20.csv", index_col=[0, 1], nrows=(365))

# convert inf to nan vaues
data[(data == np.inf) | (data == (-np.inf))] = np.nan
data.fillna(data.mean(), inplace=True)

# split into features and target
y = data.pop(TARGET)
X = data.iloc[:, -10:]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=False, random_state=1)


# CONFIGURE SPACE
space4nested = {
    "learning_rate": hp.loguniform("learning_rate", 0.001, 1),
    "n_estimators": hp.uniformint("n_estimators", 10, 500),
    # hp.uniformint returns only integers from the unifrom distribution
    "subsample": hp.uniform("subsample", 0.3, 1),
    "max_features": hp.choice("type", ["auto", "sqrt"]),
    "min_samples_split": hp.uniform("min_samples_split", 0, 1),
    "min_samples_leaf": hp.uniformint("min_samples_leaf", 1, 10),
    "max_depth": hp.uniformint("max_depth", 1, 30),
}

# Draws a few samples to see what we get from this nested configuration
for _ in range(5):
    print(pyll.stochastic.sample(space4nested))

# some default parameters for the models chosen
gb_defaults = {"loss": "quantile", "alpha": 0.5}


# OBJECTIVE FUNCTION
def hyperopt_gb_cross_val(params):
    params = {**gb_defaults, **params}
    gbf = GradientBoostingRegressor(**params)
    bst = gbf.fit(X_train, y_train)
    return_dict = {"loss": mae(y_test, bst.predict(X_test)), "status": STATUS_OK}
    return return_dict


trials = Trials()
best = fmin(hyperopt_gb_cross_val, space4nested, algo=tpe.suggest, max_evals=100, trials=trials)

# convert output from fmin into actual parameters values
best_params = space_eval(space4nested, best)

# check results and different runs
losses = trials.losses()  # losses as a list
losses_0 = trials.results  # same as before in dictionary formate
results = trials.trials  # you can confirm here that loss list comes ordered

print(f"Best parameters: {best_params}")
# save into json or csv file


# PLOTTING OPTONS

# LOSS VS. ITERATIONS
f, ax = plt.subplots(1)
xs = [t["tid"] for t in trials.trials]
ys = [t["result"]["loss"] for t in trials.trials]
ax.set_xlim(xs[0] - 10, xs[-1] + 10)
ax.scatter(xs, ys, s=20, linewidth=0.01, alpha=0.75)
ax.set_title("$Loss$ $vs$ $Iterations$ ", fontsize=18)
ax.set_xlabel("$Iteration number$", fontsize=16)
ax.set_ylabel("$Loss$", fontsize=16)
plt.show()

# HYPERPARAMETER VALUES VS. ITERATIONS
parameters = ["max_depth", "min_samples_leaf", "min_samples_split", "n_estimators", "learning_rate"]  # decision tree
cols = len(parameters)
f, axes = plt.subplots(nrows=1, ncols=cols, figsize=(20, 5))
cmap = plt.cm.jet
for i, val in enumerate(parameters):
    xs = np.array([t["misc"]["vals"][val] for t in trials.trials]).ravel()
    ys = [-t["result"]["loss"] for t in trials.trials]
    ys = np.array(ys)
    axes[i].scatter(xs, ys, s=20, linewidth=0.01, alpha=0.5, c=cmap(float(i) / len(parameters)))
    axes[i].set_title(val)
plt.show()

# from hyperopt package
plotting.main_plot_history(trials)
plotting.main_plot_histogram(trials)
# plotting.main_plot_vars(trials) # it does not work very well


# PARALLEL PLOTS
df = pd.DataFrame([{**t["misc"]["vals"], **t["result"]} for t in trials.trials])

# convert element lists into integers
for i in df.columns:
    if type(df.loc[0, i]) == list:
        df[i] = [int(x[0]) for x in df[i]]

# color can be assinged to any column and to the index if aim is to represent order of iterations = df.index
fig = px.parallel_coordinates(
    df,
    color="loss",
    dimensions=["learning_rate", "max_depth", "min_samples_leaf", "n_estimators", "loss"],
    color_continuous_scale=px.colors.diverging.Tealrose,
)

fig.show()


# %%
