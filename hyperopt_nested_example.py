"""
Hyperopt tuning experiment with two models adapted to imbalance price predictions.

Two models used with Nested Configuration space - Random forest & GBoosting

Nested Configuration space => more than one model is given for the search

NOTE: A very interesting functionality but do hyperopt for each model individually please.

"""

import numpy as np
import pandas as pd
from hyperopt import hp, fmin, tpe, pyll, STATUS_OK, Trials, space_eval
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error as mae
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor

# DATA
TARGET = "derived_system_wide_data_systemSellPrice"

# less data given to reduce processing time
data = pd.read_csv("final_dataset_18_19_20.csv", index_col=[0, 1], nrows=(365))

# convert inf to nan vaues
data[(data == np.inf) | (data == (-np.inf))] = np.nan
data.fillna(data.mean(), inplace=True)

# split into features and target
y = data.pop(TARGET)
X = data.iloc[:, -10:]

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    shuffle=False,
                                                    random_state=1)


# Random Forest model to account with quantiles
def rf_quantile(model, X_test, q):
    """ Function that allows to find quantile predicitons in random forest """
    rf_preds = []
    for estimator in model.estimators_:
        rf_preds.append(estimator.predict(X_test))
    rf_preds = np.array(rf_preds).transpose()  # One row per record.
    return np.percentile(rf_preds, q * 100, axis=1)


# CONFIGURE SPACE (fraction on the side defines amount of attention given to each option)
space4nested = hp.pchoice(
    "regressor_type",
    [
        (
            0.4,
            {
                "type": "GBoosting",
                "learning_rate": hp.loguniform("learning_rate", 0.001, 1),
                "n_estimators": hp.uniformint("n_estimators", 10, 500),
                # hp.uniformint returns only integers from the unifrom distribution
                "subsample": hp.uniform("subsample", 0.3, 1),
                "max_features": hp.choice("type", ["auto", "sqrt"]),
                "min_samples_split": hp.uniform("min_samples_split", 0, 1),
                "min_samples_leaf": hp.uniformint("min_samples_leaf", 1, 10),
                "max_depth": hp.uniformint("max_depth", 1, 30),
            },
        ),
        (
            0.6,
            {
                "type": "Random forest",
                "n_estimators": hp.uniformint("n_estimators_rf", 10, 500)),
                "min_samples_split": hp.uniform("min_samples_split_rf", 0, 1),
                "min_samples_leaf": hp.uniformint("min_samples_leaf_rf", 1, 10)),
                "max_depth": hp.uniformint("max_depth_rf", 1, 30)),
            },
        ),
    ],
)

# Draws a few samples to see what we get from this nested configuration
for _ in range(5):
    print(pyll.stochastic.sample(space4nested))

# some default parameters for the models chosen
gb_defaults = {"loss": "quantile", "alpha": 0.5}
rf_defaults = {"n_jobs": -1, "random_state": 100}


# OBJECTIVE FUNCTIONS:
def hyperopt_gb_rf_nested(params):

    model_type = params.pop("type")

    if model_type == "GBoosting":
        params = {**gb_defaults, **params}
        gbf = GradientBoostingRegressor(**params)
        bst = gbf.fit(X_train, y_train)
        return_dict = {"loss": mae(y_test, bst.predict(X_test)),
                       "status": STATUS_OK}
    else:

        params = {**rf_defaults, **params}
        rf_model = RandomForestRegressor(**params)
        rf_model.fit(X_train, y_train)
        return_dict = {"loss": mae(y_test, rf_quantile(rf_model, X_test, 0.5)),
                       "status": STATUS_OK}
    return return_dict


trials = Trials()
best = fmin(hyperopt_gb_rf_nested, space4nested,
            algo=tpe.suggest, max_evals=100, trials=trials)

# convert output from fmin to parameters values
best_params = space_eval(space4nested, best)

print(f"Best parameters: {best_params}")

# check results and different runs
losses = trials.losses()  # losses as a list
losses_0 = trials.results  # same as before in dictionary formate
results = trials.trials  # you can confirm here that loss list comes ordered
