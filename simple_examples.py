"""
Very simple examples to understand the multiple applications of the package
"""

import hyperopt
from hyperopt import hp, fmin, tpe, STATUS_OK, Trials
import hyperopt.pyll.stochastic
import time
import pickle


# EXAMPLE 1

# define an objective function
def objective(args):
    case, val = args
    if case == "case 1":
        return val
    else:
        return val ** 2

# Each of 'c1' and 'c2' only figures in the returned sample
# for a particular value of 'a'. If 'a' is 0, then 'c1' is used but
# not 'c2'. If 'a' is 1, then 'c2' is used but not 'c1'.


# define a search space
space = hp.choice("a", [("case 1", 1 + hp.lognormal("c1", 0, 1)),
                        ("case 2", hp.uniform("c2", -10, 10))])

# minimize de objective over the space
best = fmin(objective, space, algo=tpe.suggest, max_evals=100)
print(best)
print(hyperopt.space_eval(space, best))


# EXAMPLE 2
best = fmin(fn=lambda x: x ** 2, space=hp.uniform("x", -10, 10),
            algo=tpe.suggest, max_evals=100)
print(best)


# EXAMPLE 3
def objective(x):
    return {"loss": x ** 2, "status": STATUS_OK}


best = fmin(objective, space=hp.uniform("x", -10, 10),
            algo=tpe.suggest, max_evals=100)
print(best)


# EXAMPLE 4

def objective(x):
    return {
        "loss": x ** 2,
        "status": STATUS_OK,
        # -- store other results like this
        "eval_time": time.time(),
        "other_stuff": {"type": None, "value": [0, 1, 2]},
        # -- attachments are handled differently
        "attachments": {"time_module": pickle.dumps(time.time)},
    }


trials = Trials()
best = fmin(objective, space=hp.uniform("x", -10, 10),
            algo=tpe.suggest, max_evals=100, trials=trials)

print(best)
print(hyperopt.pyll.stochastic.sample(space))
