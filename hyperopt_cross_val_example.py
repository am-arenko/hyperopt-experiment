"""
Hyperopt tuning experiment with cross validation adapted to imbalance price predictions.

Instead of simple loss, cross_val_score is provided - GBoosting used

It is important to perform cross validation during tuning in time series problems as
there are times more volatiles than other.. it averages loss across different section of the data given

NOTE: always return cross_val_score with a time series split! (k-fold not the best practice)
"""

#  %%
import numpy as np
import pandas as pd
from hyperopt import hp, fmin, tpe, pyll, STATUS_OK, Trials, space_eval
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import GradientBoostingRegressor


# DATA
TARGET = "derived_system_wide_data_systemSellPrice"

# less data given to reduce processing time
data = pd.read_csv("final_dataset_18_19_20.csv",
                   index_col=[0, 1], nrows=(365))

# convert inf to nan vaues
data[(data == np.inf) | (data == (-np.inf))] = np.nan
data.fillna(data.mean(), inplace=True)

# split into features and target
y = data.pop(TARGET)
X = data.iloc[:, -10:]

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    shuffle=False,
                                                    random_state=1)


# CONFIGURE SPACE
space4nested = {
    "learning_rate": hp.loguniform("learning_rate", 0.001, 1),
    "n_estimators": hp.uniformint("n_estimators", 10, 500),
    "subsample": hp.uniform("subsample", 0.3, 1),
    "max_features": hp.choice("type", ["auto", "sqrt"]),
    "min_samples_split": hp.uniform("min_samples_split", 0, 1),
    "min_samples_leaf": hp.uniformint("min_samples_leaf", 1, 10),
    # hp.uniformint returns only integers from the unifrom distribution
    "max_depth": hp.uniformint("max_depth", 1, 30),
}

# Draws a few samples to see what we get from this nested configuration
for _ in range(5):
    print(pyll.stochastic.sample(space4nested))

# some default parameters for the models chosen
gb_defaults = {"loss": "quantile", "alpha": 0.5}


# OBJECTIVE FUNCTION
def hyperopt_gb_cross_val(params):

    params = {**gb_defaults, **params}
    gbf = GradientBoostingRegressor(**params)
    cross_score = -cross_val_score(gbf, X_train, y_train,
                                   scoring="neg_mean_absolute_error",
                                   cv=5).mean()
    return_dict = {
        "loss": cross_score,
        "status": STATUS_OK,
    }

    return return_dict


trials = Trials()
best = fmin(hyperopt_gb_cross_val, space4nested,
            algo=tpe.suggest, max_evals=100, trials=trials)

# convert output from fmin into actual parameters values
best_params = space_eval(space4nested, best)

# check results and different runs
losses = trials.losses()  # losses as a list
losses_0 = trials.results  # same as before in dictionary formate
results = trials.trials  # you can confirm here that loss list comes ordered

print(f"Best parameters: {best_params}")
# save into json or csv file

# %%
